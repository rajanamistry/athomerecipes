import API from "@/services/API";

export const authenticate = (userDetails) => {
  const url = "/authenticate";
  return API().post(url, userDetails);
};

export const isLoggedIn = () => {
  if (document.cookie) {
    return document.cookie.indexOf("login=true") !== -1;
  } else {
    return false;
  }
};
