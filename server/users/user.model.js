var mongoose = require("mongoose");
var userConstants = require("./user.constant");
var Schema = mongoose.Schema;

var userSchema = new Schema({
  name: String,
  email: String,
  password: String,
  role: {
    type: String,
    enum: [userConstants.USER, userConstants.ADMINISTRATOR],
  },
});

module.exports = mongoose.model("User", userSchema);
