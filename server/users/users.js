const User = require("./user.model");

module.exports.authenticate = async (username, password) => {
  const params = { email: username };
  console.log(params);
  const [user] = await User.find(params, "_id name password email role");
  if (user.password === password) {
    return user;
  } else {
    throw "Wrong login id or password";
  }
};

module.exports.createUser = async (newUser) => {
  const { name, email, password, role } = newUser;
  var userModel = new User({
    name: name,
    email: email,
    password: password,
    role: role,
  });
  const result = await userModel.save();
  return result;
};

module.exports.getUsers = async () => {
  const result = await User.find({}, "_id name email role").sort({ _id: -1 });
  return result;
};

module.exports.getUser = async (id) => {
  const params = { _id: id };
  const result = await User.find(params, "_id name email role").sort({
    _id: -1,
  });
  return result;
};

module.exports.searchUser = (params) => {
  return User.find(params, "_id name email role").exec();
};

module.exports.updateUser = (id, userDetails) => {
  const { name, email, password, role } = userDetails;
  User.findById(id, "_id name password email role", (error, user) => {
    if (error) {
      console.error(error);
      throw error;
    }

    user.name = name;
    user.email = email;
    if (password) {
      user.password = password;
    }
    user.role = role;

    user.save((error, result) => {
      if (error) {
        console.error(error);
        throw error;
      }
      console.log(result);
    });
  });
};

module.exports.deleteUser = (id) => {
  User.findByIdAndDelete(id, (error, user) => {
    if (error) {
      console.error(error);
      throw error;
    }
    console.log(user.name);
  });
};
