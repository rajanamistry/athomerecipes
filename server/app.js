const bodyParser = require("body-parser");
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");
const express = require("express");
const db = require("./config/database");
const userAPI = require("./users/users");
const recipeAPI = require("./recipe/recipe");

/**
 * Connect the database
 */
db.connect();

/**
 * Configure the app
 */
app = express();
app.use(helmet()).use(morgan("combined")).use(bodyParser.json()).use(cors());

/**
 * Authenticate
 */
 app.post("/authenticate", (req, res) => {
  const {username, password} = req.body;
  try {
    userAPI.authenticate(username, password).then((user) => {
      if (user) {
        res.send({ result: user });
        return;
      }
    });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Create User
 */
app.post("/users", (req, res) => {
  const userModel = req.body;
  try {
    userAPI.searchUser({ email: userModel.email }).then((users) => {
      isUserPresent = users && users.length > 0;
      if (isUserPresent) {
        res.send({ error: "User already registered" });
        return;
      }
      userAPI.createUser(userModel).then((result) => {
        res.send({ result: result });
      });
    });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Get all users
 */
app.get("/users", (req, res) => {
  try {
    userAPI.getUsers().then((users) => {
      res.send({ result: users });
    });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Get user details
 */
app.get("/users/:id", (req, res) => {
  const id = req.params.id;
  try {
    userAPI.getUser(id).then((user) => {
      user = user && user.length > 0 ? user[0] : {};
      res.send({ result: user });
    });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Update user details
 */
app.put("/users/:id", (req, res) => {
  try {
    const id = req.params.id;
    userAPI.updateUser(id, req.body);
    res.send({ result: req.body });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Delete user
 */
app.delete("/users/:id", (req, res) => {
  const id = req.params.id;
  try {
    userAPI.deleteUser(id);
    res.send({ result: "User successfully deleted" });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Create recipe
 */
app.post("/recipes", (req, res) => {
  try {
    recipeAPI.createRecipe(req.body).then((recipe) => {
      res.send({ result: recipe });
    });
  } catch (error) {
    console.error(error);
    res.send({ error: error });
  }
});

/**
 * Get all recipes
 */
app.get("/recipes", (req, res) => {
  try {
    recipeAPI.getRecipes().then((recipes) => {
      res.send({ result: recipes });
    });
  } catch (error) {
    console.log(error);
    res.send({ error: error });
  }
});

/**
 * Get recipe details
 */
app.get("/recipes/:id", (req, res) => {
  const id = req.params.id;
  try {
    recipeAPI.getRecipe(id).then((recipe) => {
      res.send(recipe);
    });
  } catch (error) {
    console.log(error);
    res.send({ error: error });
  }
});

/**
 * Update recipe details
 */
app.put("/recipes/:id", (req, res) => {
  try {
    const id = req.params.id;
    recipeAPI.updateRecipe(id, req.body);
    res.send({ result: req.body });
  } catch (error) {
    console.log(error);
    res.send({ error: error });
  }
});

/**
 * Delete recipe
 */
app.delete("/recipes/:id", (req, res) => {
  const id = req.params.id;
  try {
    recipeAPI.deleteRecipe(id);
    res.send({ result: "Recipe successfully deleted" });
  } catch (error) {
    console.log(error);
    res.send({ error: error });
  }
});

/**
 * Add listerner to the app
 */
app.listen(process.env.PORT || 8082);
