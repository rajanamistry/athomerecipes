const Recipe = require("./recipe.model");

module.exports.createRecipe = async (recipe) => {
  const { title, author, description, ingredients } = recipe;

  var newRecipe = new Recipe({
    title: title,
    author: author,
    description: description,
    likes: 0,
    ingredients: ingredients,
  });
  const result = await newRecipe.save();
  return result;
};

module.exports.getRecipes = async () => {
  const result = await Recipe.find(
    {},
    "_id title author likes description"
  ).sort({
    _id: -1,
  });
  return result;
};

module.exports.getRecipe = async (id) => {
  const params = { _id: id };
  const result = await Recipe.find(
    params,
    "_id title author likes description ingredients"
  ).sort({ _id: -1 });
  return result;
};

module.exports.searchRecipe = async (params) => {
  const result = await Recipe.find(
    params,
    "_id title author likes description ingredients"
  ).sort({ _id: -1 });
  return result;
};

module.exports.updateRecipe = (id, recipeDetails) => {
  const { title, description, author, likes, ingredients } = recipeDetails;
  Recipe.findById(
    id,
    "_id title author likes description ingredients",
    (error, recipe) => {
      if (error) {
        console.error(error);
        throw error;
      }

      recipe.title = title;
      recipe.author = author;
      recipe.description = description;
      recipe.likes = likes;
      recipe.ingredients = ingredients;

      recipe.save((error, result) => {
        if (error) {
          console.error(error);
          throw error;
        }
        console.log(result);
      });
    }
  );
};

module.exports.deleteRecipe = (id) => {
  Recipe.findByIdAndDelete(id, (error, recipe) => {
    if (error) {
      throw error;
    }
    console.log(recipe.title);
  });
};
