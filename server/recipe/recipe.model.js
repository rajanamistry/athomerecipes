var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var recipeSchema = new Schema({
  title: String,
  description: String,
  author: {},
  likes: Number,
  ingredients: [{ name: String, measure: Number, unit: String }],
});

module.exports = mongoose.model("Recipe", recipeSchema);
